//instance creation
resource "google_compute_instance" "instance-1" {
  name         = "test-instance"
  machine_type = var.machine_type
  zone         = var.region

  tags = var.names

  boot_disk {
    initialize_params {
      image = var.instance_os
    }
  }

  // Local SSD disk
  scratch_disk {
    interface = var.interface
  }

  network_interface {
    network = var.network
  }

  metadata_startup_script = var.metadata_script

  service_account {
    scopes = var.scopes
  }
}