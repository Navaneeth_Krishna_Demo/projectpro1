provider "google" {
  credentials = file("CREDENTIALS.json")
  project     = var.project_id
  region      = var.region
}