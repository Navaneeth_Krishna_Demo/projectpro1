project_id = "terraform-init-290308"
region = "us-central1-a"
machine_type = "n1-standard-1"
instance_os = "debian-cloud/debian-9"
interface = "SCSI"
network = "default"
metadata_script = "echo hi > /test.txt"
scopes = ["userinfo-email", "compute-ro", "storage-ro"]
names = ["foo", "bar"]