variable "project_id"{
    description = "Project id declaration"
    type = string
}

variable "region"{
    description = "Project region declaration"
    type = string
}
variable "machine_type"{
    description = "Project machine-type declaration"
    type = string
}

variable "instance_os"{
    description = "Project instance-os declaration"
    type = string
}

variable "interface"{
    description = "Project interface declaration"
    type = string
}

variable "network"{
    description = "Project network declaration"
    type = string
}

variable "metadata_script"{
    description = "Project metadata_script declaration"
    type = string
}

variable "scopes"{
    description = "Project scopes declaration"
    type = list
}

variable "names"{
    description = "Project name declaration"
    type = list
}
